<?php

namespace Caravel\Service\Curl;

use \SimpleXMLElement;

class Manager
{
    /** @var string */
    private $serviceUrl;

    /** @var string */
    private $userName;

    /** @var string */
    private $password;

    public function __construct($serviceUrl, $username, $password)
    {
        $this->serviceUrl = $serviceUrl;
        $this->username = $username;
        $this->password = $password;
    }

    public function getCandidateSchema()
    {
        return $this->send('candidate/schema');
    }

    public function getRequisitionSchema()
    {
        return $this->send('position/schema');
    }

    public function getList($type, $id) {
        $endpoint = sprintf('ref/%s/?params=type:%s', $type, $id);
        return $this->send($endpoint);
    }

    private function send($endpoint)
    {
        $curl = curl_init($this->serviceUrl . $endpoint);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_USERPWD, $this->username . ':' . $this->password);
        $curl_response = curl_exec($curl);
        curl_close($curl);

        return new SimpleXMLElement($curl_response);
    }
}
