<?php

namespace Caravel\Service\File;

class Uploader
{
    private $id;

    private $file;


    private $fileType;

    private $allowedTypes = array('text/csv');

    private $uploadDir;

    public function __construct($id, $file, $fileType, $uploadDir)
    {
        $this->id = $id;
        $this->fileType = $fileType;
        $this->uploadDir = $uploadDir;
        $this->file = $file;
    }

    public function isFileValid()
    {
        return in_array($this->file['type'], $this->allowedTypes);
    }

    public function moveFile()
    {
        $destinationFile = sprintf('%s%s-%s.csv', $this->uploadDir, $this->fileType, $this->id);
        if (file_exists($destinationFile)) {
            unlink($destinationFile);
        }
        move_uploaded_file($this->file['tmp_name'], $destinationFile);
    }

    public function saveFile()
    {
        if (!$this->isFileValid()) {
            return array(
                'code' => 400,
                'message' => sprintf(
                    'The type of the file is not correct (should be %s)',
                    implode(', ', $this->allowedTypes)
                ),
            );
        }

        $this->moveFile();
        return array(
            'code' => 100,
            'message' => 'The file has been successfully uploaded',
        );
    }
}
