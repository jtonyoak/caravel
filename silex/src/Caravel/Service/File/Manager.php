<?php

namespace Caravel\Service\File;

use parseCSV;

class Manager
{
	private $file;
	private $uploadDir;
	private $csv;
	private $totalRows;

	public function __construct($file, $uploadDir)
	{
		$this->file = $file;
		$this->uploadDir = $uploadDir;
		$this->csv = new parseCSV();
	}

	public function parse()
	{
		$this->csv->parse($this->uploadDir . $this->file);
		$this->totalRows = count($this->csv->data) - 1;
		return $this;
	}

	private function fileExists()
	{
		return file_exists($this->uploadDir . $this->file);
	}

	public function getTitles()
	{ 
		if ($this->fileExists()) {
			return $this->csv->titles;
		}
	}

	// get unique value list
	public function getColumnList($columnName)
	{
		$return = array();
		foreach ($this->csv->data as $row) {
			if (!in_array($row[$columnName], $return)) {
				$return[] = $row[$columnName];
			}
		}
		return $return;
	}

	public function getSample()
	{
		if ($this->fileExists()) {
			// return the first, the middle and the last rows
			//var_dump($this->csv);
			return array(
				$this->csv->data[0], 
				$this->csv->data[floor($this->totalRows * 0.25)], 
				$this->csv->data[floor($this->totalRows * 0.5)], 
				$this->csv->data[floor($this->totalRows * 0.75)], 
				$this->csv->data[$this->totalRows]
			);
		}
	}

	public function getGenerated()
	{
		/*if ($this->fileExists()) {
			return $this->csv->data;
		}*/
		return 'test';
	}

	public function getResponse() 
	{
		return array(
	        'status' => 100,
	        'columns' => $this->getTitles(),
	        'sample' => $this->getSample(),
	        'generated' => $this->getGenerated()
    	);
	}
}
