<?php

namespace Caravel\Service\File;

use parseCSV;

class Transformation 
{
	private $fileName;
	private $transformationDatas;
	private $uploadDir;
	private $transDir;

	private $csv;

	public function __construct($fileName, $transformationDatas, $uploadDir, $transDir)
	{
		$this->fileName = $fileName;
		$this->transformationDatas = json_decode($transformationDatas, true);
		$this->uploadDir = $uploadDir;
		$this->transDir = $transDir;
	}

	public function init()
	{
		if (file_exists($this->transDir . $this->fileName)) {
			unlink($this->transDir . $this->fileName);
		}
		$this->csv = new parseCSV();
		$this->csv->parse($this->uploadDir . $this->fileName);
	}


	public function processLine($row)
	{
		$transformations = $this->transformationDatas['transformation'];

		$newRow = array();
		$i = 1;
		foreach ($transformations as $col => $datas) {
			if (!empty($datas[0])) {
				if ($datas[1] == "string") {
					$newRow[] = $row[$col];
				} elseif ($datas[1] == "list") {
					$mapping = $this->transformationDatas['mappings'][$datas[2]];
					$newRow[] = isset($mapping[$row[$col]]) ? $mapping[$row[$col]] : '';
				} else {
					$newRow[] = '';
				}
			}
		}
		return $newRow;
	}

	public function updateTitles($titles)
	{
		$transformations = $this->transformationDatas['transformation'];
		$newTitles = array();
		foreach ($transformations as $col => $datas) {
			if (!empty($datas[0])) {
				$newTitles[] = $datas[0];
			}
		}
		return $newTitles;
	}

	/**
	 * This method will fetch each line of the csv file to reformat the file
	 * depending on the given mapping information
	 */
	public function process()
	{
		$newTitles = $this->updateTitles($this->csv->titles);
		$this->csv->titles = $newTitles;
		for ($i=0; $i<count($this->csv->data); $i++) {
			$row = $this->processLine($this->csv->data[$i]);
			$this->csv->data[$i] = $row;
		}

		$this->csv->save($this->transDir . $this->fileName);		
	}
}
