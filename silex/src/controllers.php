<?php
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Caravel\Service\File\Uploader as FileUploader;
use Caravel\Service\File\Manager as FileManager;
use Caravel\Service\File\Transformation as FileTransform;

// Index
$app->get('/', function () use ($app) {
    return $app['templating']->render(
        'index.html.php',
        array()
    );
});

/*
    API index documentation
*/
$app->get('/api/', function () use ($app) {
    return $app['twig']->render(
        'api.index.html.twig',
        array(
            'endpoints' => array(
                'POST api/file/{id}',
                'GET api/file/{id}',
                'GET api/file/{id}/header',
                'GET api/candidate/schema',
            ),
        )
    );
});

// Get the candidate schema from the webservice
$app->get('/api/candidate/schema/', function () use ($app) {
    $xml = $app['curl']->getCandidateSchema();
    return $app->json($xml, 200, ['Content-Type' => 'application/json']);
});

// Get the candidate schema from the webservice
$app->get('/api/requisition/schema/', function () use ($app) {
    $xml = $app['curl']->getRequisitionSchema();
    return $app->json($xml, 200, ['Content-Type' => 'application/json']);
});

// upload a file
$app->post('api/file/{id}/{type}/', function ($id, $type) use ($app) {
    $files = array();

    if (count($_FILES) === 1) {
        $file = array_values($_FILES)[0];
        $fileUploader = new FileUploader($id, $file, $type, $app['upload.dir']);
        $response = $fileUploader->saveFile();
    } else {
        $response = array(
            'code' => 400,
            'message' => 'There was a problem during the upload',
        );
    }

    return $app->json($response, 200, ['Content-Type' => 'application/json']);
});

$app->get('api/file/{id}/{type}/', function ($id, $type) use ($app) {
    $file = sprintf('%s-%s.csv', $type, $id);
    $fileManager = new FileManager($file, $app['upload.dir']);
    $fileManager->parse();
    $response = array(
        'code' => 100,
        'columns' => $fileManager->getTitles(),
        'sample' => $fileManager->getSample(),
        'generated' => $fileManager->getGenerated(),
    );

    return $app->json($response, 200, ['Content-Type' => 'application/json']);
});

$app->get('api/file/{id}/{type}/list/{colcsv}/', function ($id, $type, $colcsv) use ($app) {
    $file = sprintf('%s-%s.csv', $type, $id);
    $fileManager = new FileManager($file, $app['upload.dir']);
    $fileManager->parse();
    $response = array(
        'code' => 200,
        'value' => $fileManager->getColumnList($colcsv),
    );
    return $app->json($response, 200, ['Content-Type' => 'application/json']);
});


// get lists
$app->get('api/list/{type}/{id}/', function ($type, $id) use ($app) {
    $xml = $app['curl']->getList($type, $id);
    return $app->json($xml, 200, ['Content-Type' => 'application/json']);
});

// transformation endpoint
$app->post('api/transformation/{id}/{type}/', function ($id, $type, Request $request) use ($app) {
    $datas = $request->get('tranformationData');
    $fileName = sprintf('%s-%s.csv', $type, $id);
    if (file_exists($app['upload.dir'] . $fileName)) {
        $service = new FileTransform($fileName, $datas, $app['upload.dir'], $app['transformation.dir']);
        $service->init();
        $service->process();

        $url = sprintf(
            'http://%s%s/transformations/%s',
            $request->getHost(),
            ($request->getPort() ? ':' . $request->getPort() : ''),
            $fileName
        );

        $response = array(
            'code' => 100,
            'message' => 'The file is successfully transformed',
            'path' => realpath($app['transformation.dir'] . $fileName),
            'url' => $url,
        );
    } else {
        $response = array(
            'code' => 400,
            'message' => 'The requested file does not exists'
        );
    }
    return $app->json($response, 200, ['Content-Type' => 'application/json']);
});
