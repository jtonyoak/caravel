'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');

var pathCSS   = './web/assets/css';
var pathFonts = './web/assets/fonts';
var pathJS    = './web/assets/js';
var pathTpl   = './web/assets/tpl';

function compileCSS() {
    return gulp.src(['./assets/css/main.scss'])
        .pipe(concat('out.css'))
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest(pathCSS));
}

function collectFonts() {
    return gulp.src([
        './node_modules/bootstrap-sass/assets/fonts/bootstrap/*.@(eot|svg|ttf|woff|woff2)',
        './node_modules/font-awesome/fonts/*.@(eot|svg|ttf|woff|woff2|otf)'])
        .pipe(gulp.dest(pathFonts));
}

function collectTemplates() {
    return gulp.src([
        './assets/tpl/*.mst'])
        .pipe(gulp.dest(pathTpl));
}

function compileJS() {
    return gulp.src([
        './node_modules/jquery/dist/jquery.min.js',
        './node_modules/bootstrap-sass/assets/javascripts/bootstrap.min.js',
        './node_modules/mustache/mustache.min.js',
        './node_modules/bootbox/bootbox.min.js',
        './assets/js/**/*.js'])
        .pipe(concat('out.js'))
        .pipe(gulp.dest(pathJS));
}

gulp.task('compile:css', function () {
    return compileCSS();
});

gulp.task('collect:fonts', function () {
    return collectFonts();
});

gulp.task('collect:tpl', function () {
    return collectTemplates();
});

gulp.task('compile:js', function () {
    return compileJS();
});


gulp.task('default', ['compile:css', 'compile:js', 'collect:fonts', 'collect:tpl'], function () {});
