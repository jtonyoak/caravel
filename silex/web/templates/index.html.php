<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>title</title>
        <link rel="stylesheet" href="/assets/css/out.css">
        <script src="/assets/js/out.js"></script>
        
        <script type="text/javascript">
            $(document).ready(function(){
                var caravel = new Caravel('.application');
            });
        </script>

    </head>
    
    <body>
        
        <br /><br /><br />
        <div class="application"></div>

    </body>
</html>