var Caravel = function(_container){
    "use strict"
    
    /* JS style :) */
    var that = this;

    var DEFAULT = {},
        config = {},
        forms_map = {},
        lists_map = {};

    /* Container */
    var container = $(_container);

    if(container.length != 1){
        console.log('nope');
    }

    /* Generate unique ID */
    var id =    Math.floor((1 + Math.random()) * 0x10000000000).toString(16);

    /* Default values */

    DEFAULT.forms = [
        {
            id: 'candidate',
            label: 'Candiates',
            props:{},
            resource: {},
            selected: true,
            status: false,
            supported: true
        },
        {
            id: 'requisition',
            label: 'Requisitions',
            props: {},
            resource: {},
            supported: true,
            selected: false,
            status: false,
            resource: {}
        },
        {
            id: 'toto',
            label: 'Toto',
            supported: false,
            selected: false,
            resource: {}
        }];

    DEFAULT.lists = [
        {
            id: 'listeA',
            label: 'Liste A',
            supported: true,
            selected: true
        },
        {
            id: 'listeB',
            label: 'Liste B',
            supported: true,
            selected: false
        },
        {
            id: 'listeC',
            label: 'Liste C',
            supported: true,
            selected: false
        },
        {
            id: 'listeD',
            label: 'Liste D',
            supported: true,
            selected: false
        }];


    /* Config */

    $.extend(config, DEFAULT);

    config.system = '';
    config.user = 'tderambure@profilsoft.com';


    this.getID = function() {
        return id;
    }

    this.setID = function(_id) {
        id = _id;
    }

    /* UI */
    this.getUI = function() {
        return UI;
    }

    this.getContainer = function() {
        return container;
    }

    /* Form */
    this.getForms = function() {
        return config.forms;
    }

    this.getForm = function(id) {
        if(config.forms[id]){
            return config.forms[id];  
        } else if (config.forms[forms_map[id]]){
            return config.forms[forms_map[id]];
        }
        return null;
    }

    /* Lists */
    this.getLists = function() {
        return config.lists;
    }

    this.getList = function(id) {
        if(config.lists[id]){
            return config.lists[id];  
        } else if (config.lists[lists_map[id]]){
            return config.lists[lists_map[id]];
        }
        return null;
    }

    /* Config */
    this.getConfig = function() {
        return config;
    }

    this.setConfig = function(_config) {
        config = _config;
    }

    /* Init form */
    function mapForms (){
        $.each(config.forms, function(i, j) {
            forms_map[j.id] = i;
        });
    }

    function mapLists (){
        $.each(config.lists, function(i, j) {
            lists_map[j.id] = i;
        });
    }


    /* Watchers */
    /*$.each(this.getForms(), function(i, _form){
        that.getForm(i).ressource.watch('valid', function (prop, oldValue, newValue){
            if(newValue === true){
                $('.step-import-lists .import-from-forms .form-' + _form.id).show();
            }

            if(newValue === false){
                $('.step-import-lists .import-from-forms .form-' + _form.id).hide();
            }

            return newValue;
        });
    });*/
    

    /* Templates */

    var templatesToLoad = ['steps.import.forms','steps.import.lists']; 
    var templates = {};

    this.loadTemplates = function(_templates) {
        $.each(_templates, function(i, _template) {
            that.getTemplate(_template);
        });
    }

    this.loadTemplate = function(_id) {
        return $.ajax({
            url: '/assets/tpl/' + _id + '.mst',
            async: false,
            complete: function(_template) {
                return _template;
            }
        });
    }


    this.getTemplates = function() {
        return templates;
    }

    this.getTemplate = function(_id) {
        if (templates[_id]) {
            return templates[_id];
        } else {
            var reponse = this.loadTemplate(_id);
            if (reponse.status = 200){
                templates[_id] = reponse.responseText;
                return templates[_id];
            } else {
                return templates[_id] = null;
            }
        }
    }

    /* Init */
    mapForms();
    mapLists();
    this.loadTemplates();

    /* Attach UI */
    var UI = new CaravelUI(this);

}
