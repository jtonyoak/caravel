var CaravelUI = function (_parent){
    "use strict"
    
    /* App */
    var app = _parent;
    var that = this;
    
    this.getApp = function() {
        return app;
    }

    /* Container */
    var container = app.getContainer();    
    container.addClass('caravelUI');

    /* Templates */

    var steps = {
        application: { element: $('<div class="step-application" />') },
        options: { element: $('<div class="step-options" />') },
        imports: { element: $('<div class="step-imports" />') },
        mapping: { element: $('<div class="step-mapping" />') }
    }

    this.getStep = function(_id) {
        if (steps[_id]) return steps[_id];
        else return null;
    }

    this.getSteps = function() {
        return steps;
    }

    function insertSteps() {
        $.each(steps, function(i, step) {
            container.append(step.element);
        });
    }

    function initSteps() {
        var requiredTemplates = [];

        $.each(steps, function(i, step) {
            /* Get constructor name */
            var constructor = i.charAt(0).toUpperCase() + i.slice(1);

            /* Create and Build step */
            step.object = new Steps[constructor](that);
            requiredTemplates = requiredTemplates.concat(step.object.getRequiredTemplates());
        });

        /* Load required templates */
        app.loadTemplates(requiredTemplates);

        $.each(steps, function(i, step) {
            step.object.hide();
        });

        $('body').dblclick(function(){
            console.log(JSON.stringify(app.getConfig()));
        })
            
        app.setID('121cfe3709a');
        app.setConfig($.parseJSON(
            '{"forms":[{"id":"candidate","label":"Candiates","props":{"Prop0":{"type":"text","label":{"en":"Unique ID"}},"Prop4":{"type":"text","label":{"en":"Last Name"},"maxlength":"50"},"Prop6":{"type":"text","label":{"en":"First Name"},"maxlength":"50"},"Prop9":{"type":"text","label":{"en":"Address Line 1"},"maxlength":"100"},"Prop10":{"type":"text","label":{"en":"Address Line 2"},"maxlength":"100"},"Prop11":{"type":"text","label":{"en":"Zip Code"},"maxlength":"10"},"Prop12":{"type":"text","label":{"en":"City"},"maxlength":"50"},"Prop14":{"type":"ref","label":{"en":"Country"},"type-noeud":"1","multiple":"false"},"Prop15":{"type":"text","label":{"en":"Email"},"maxlength":"50"},"Prop16":{"type":"text","label":{"en":"Home Phone"},"maxlength":"30"},"Prop17":{"type":"text","label":{"en":"Mobile Phone"},"maxlength":"30"},"Prop22":{"type":"liste","label":{"en":"Work Experience"},"type-liste":"4","multiple":"false"},"Prop23":{"type":"liste","label":{"en":"Education Level"},"type-liste":"2","multiple":"false"},"Prop65":{"type":"niveau-langue-liste"},"Prop66":{"type":"niveau-langue-liste"},"Prop93":{"type":"ref","label":{"en":"State"},"type-noeud":"2","multiple":"false","filtre-parent":"Prop137"},"Prop102":{"type":"liste","label":{"en":"Current Work Status"},"type-liste":"5","multiple":"false"},"Prop103":{"type":"liste","label":{"en":"Skills"},"type-liste":"10","multiple":"true"},"Prop111":{"type":"ref","label":{"en":"Country"},"type-noeud":"1","multiple":"true"},"Prop112":{"type":"ref","label":{"en":"State"},"type-noeud":"2","multiple":"true","filtre-parent":"Prop137"},"Prop113":{"type":"ref","label":{"en":"City"},"type-noeud":"3","multiple":"true","filtre-parent":"Prop137"},"Prop114":{"type":"ref","label":{"en":"Zip"},"type-noeud":"4","multiple":"false","filtre-parent":"Prop137"},"Prop116":{"type":"ref","label":{"en":"Department"},"type-noeud":"9","multiple":"false"},"Prop117":{"type":"ref","label":{"en":"Job Desired"},"type-noeud":"10","multiple":"true","filtre-parent":"Prop137"},"Prop118":{"type":"liste","label":{"en":"Employment Type"},"type-liste":"3","multiple":"false"},"Prop121":{"type":"text","label":{"en":"Salary Expectations"},"maxlength":"50"},"Prop190":{"type":"bool","label":{"en":"Authorized to work the U.S."}},"Prop191":{"type":"text","label":{"en":"Comments"},"maxlength":"2000"},"Prop199":{"type":"liste","label":{"en":"Availability"},"type-liste":"1","multiple":"false"},"Prop250":{"type":"bool","label":{"en":"Internal Candidate"}},"flag1":{"type":"int"},"flag2":{"type":"int"},"flag3":{"type":"int"},"flag4":{"type":"int"},"flag5":{"type":"int"},"receivemailing":{"type":"bool","label":{"en":"Receive Our Communications by Email"}}},"resource":{"columns":["id","prenom","nom","email"],"sample":[{"id":"1","prenom":"thomas","nom":"der","email":"tder@pfs.com"},{"id":"2","prenom":"tony","nom":"jur","email":"tjur@pfs.com"},{"id":"3","prenom":"jerem","nom":"gas","email":"jgas@pfs.com"},{"id":"4","prenom":"clem","nom":"her","email":"cher@pfs.com"},{"id":"5","prenom":"choris","nom":"ber","email":"cber@pfs.com"}]},"selected":true,"status":100,"supported":true},{"id":"requisition","label":"Requisitions","props":{},"resource":{},"supported":true,"selected":false,"status":false},{"id":"toto","label":"Toto","supported":false,"selected":false,"resource":{}}],"lists":[{"id":"listeA","label":"Liste A","supported":true,"selected":true},{"id":"listeB","label":"Liste B","supported":true,"selected":false},{"id":"listeC","label":"Liste C","supported":true,"selected":false},{"id":"listeD","label":"Liste D","supported":true,"selected":false}],"system":"","user":"tderambure@profilsoft.com"}'));

        steps['mapping'].object.show();

    }

    insertSteps();
    initSteps();

}
