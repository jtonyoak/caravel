var Utils = window.Utils || {};

Utils.Table = function(_ui) {

    var cols = [];
    var rows = [];

    this.setCols = function(_cols) {
        cols = _cols;
    }

    this.setRows = function(_rows) {
        rows = _rows;
    }

    this.render = function() {
        var r = '<table class="table table-condensed">';
        r += '<thead>';
        r += '<tr>';

        $.each(cols, function(i, j) {
            r += '<th>' + j + '</th>';
        });

        r += '</tr>';
        r += '</thead>';


        r += '<tbody>';

        $.each(rows, function(i, j) {
            r += '<tr>';

            $.each(j, function(k, l) {
                r += '<td> ' + l+ '</td>';
            });

            r += '</tr>';
        });

        r += '</tbody>';
        r += '</table>';

        return r;
    }
}