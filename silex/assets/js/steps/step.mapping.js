var Steps = window.Steps || {};


Steps.Mapping = function(_ui) {

    var that = this;

    $.extend(this, new Steps.Base());

    this.requiredTemplates = ['steps.mapping', 'steps.mapping.tabs', 'steps.mapping.tabscontent', 'steps.mapping.row'];

    this.ui = _ui,
    this.app = this.ui.getApp(),
    this.id = 'mapping';
    
    this.elements = {};
    this.elements.content = this.ui.getStep(this.id).element;

    if (! this.elements.content) return false;

    this.build = function() {

        /* Reset html */

        this.elements.content.html('');

        /* Insert main template */

        var rendered = Mustache.render(this.app.getTemplate('steps.mapping'), {
            forms: this.app.getForms(),
            lists: this.app.getLists()
        });

        this.elements.content.append(rendered);

        /* Retreive used elements */

        this.elements.container   = $('.container', this.elements.content);
        this.elements.panel       = $(' > .row > .panel', this.elements.container);
        this.elements.panelBody   = $('.panel-body', this.elements.panel);
        this.elements.panelFooter = $('.panel-footer', this.elements.panel);
        this.elements.btnPrevStep = $('[data-toggle=step-mapping-btn-prev-step]', this.elements.panelFooter);
        this.elements.btnNextStep = $('[data-toggle=step-mapping-btn-next-step]', this.elements.panelFooter);

        this.elements.tabs          = $('.nav-tabs', this.elements.panelBody);
        this.elements.tabsContent   = $('.tab-content', this.elements.panelBody);
        
        /* */

        this.elements.btnPrevStep.click(function() {
            that.goToPrevStep();
        });

        this.elements.btnNextStep.click(function() {
            that.validate();
        });

        $.each(this.app.getForms(), function(i, j){
            if(j.selected) {

                var props = $.ajax({
                    url: '/api/candidate/schema/',
                    type: 'get',
                    cache: false,
                    async: false
                }).responseText;

                that.app.getForm(i).props = $.parseJSON(props).result;

                var tab = Mustache.render(that.app.getTemplate('steps.mapping.tabs'), j);
                that.elements.tabs.append(tab);

                var tabcontent = Mustache.render(that.app.getTemplate('steps.mapping.tabscontent'), j);
                that.elements.tabsContent.append(tabcontent);

                var tabcontent = $('#tab-content-form-' + j.id, that.elements.tabsContent);
                var table = $('table', tabcontent);


                var propsArray = [];

                $.each(that.app.getForm(i).props, function(k, l){
                    if ('label' in l && 'en' in l.label) {
                        propsArray.push({
                            label : l.label.en,
                            prop : k, 
                            type : l.type 
                        });
                    }
                });

                $.each(j.resource.columns, function(k, l) {
                    
                    var values = [];
                    $.each(j.resource.sample, function(m, n) {
                        values.push(n[l])
                    })

                    var row = Mustache.render(that.app.getTemplate('steps.mapping.row'),{
                        formId: j.id,
                        index: k,
                        label: l,
                        values: values,
                        props: propsArray
                    });
                    table.find('tbody').append(row);

                })
            }
        });

        $('a', this.elements.tabs).click(function(e) {
            e.preventDefault()
            $(this).tab('show')
        });

        $('a:first-child', this.elements.tabs).trigger('click');

    }

    /* Kludge: Not the best way to achieve that but yeah, hackathon pressure ! */

    this.check = function() {
        var errorNb = 0;
        
        /* Each forms */
        $.each(this.app.getForms(), function(i, j){
            if(j.selected) {

                /* Get elements */
                var tab = $('#tab-form-' + j.id);
                var errorTab = $('.placement-error', tab);
                var errorTabNb = 0;

                $.each(j.resource.columns, function(k, l) {

                    /* Get elements */
                    var row = $('[data-toggle=step-mapping-' + j.id + '-' + l + ']');
                    var select = $('select', row);
                    var errorRow = $('.placement-error', row);
                    
                    /* If select has no value */
                    if (! select.val()) {
                        errorRow.html('<i class="fa fa-exclamation mark-error"></i>');
                        errorTabNb++;
                        errorNb++;
                    } else {
                        errorRow.html('');
                    }
                });

                /* If at least 1 error on any select on tab */
                if (errorTabNb > 0) {
                    errorTab.html('<i class="fa fa-exclamation mark-error"></i>');    
                } else {
                    errorTab.html('');
                }

            };
        });
    }

    this.validate = function() {
        var errorNb = 0;
        var mapping = {};

        /* Each forms */
        $.each(this.app.getForms(), function(i, j){
            if(j.selected) {

                mapping[j.id] = {};

                /* Get elements */
                var tab = $('#tab-form-' + j.id);
                var errorTab = $('.placement-error', tab);
                var errorTabNb = 0;

                $.each(j.resource.columns, function(k, l) {

                    /* Get elements */
                    var row = $('[data-toggle=step-mapping-' + j.id + '-' + l + ']');
                    var select = $('select', row);
                    var errorRow = $('.placement-error', row);
                    
                    /* If select has no value */
                    if (! select.val()) {
                        errorRow.html('<i class="fa fa-exclamation mark-error"></i>');
                        errorTabNb++;
                        errorNb++;
                    } else {
                        mapping[j.id][l] = select.val();
                        errorRow.html('');
                    }
                });

                /* If at least 1 error on any select on tab */
                if (errorTabNb > 0) {
                    errorTab.html('<i class="fa fa-exclamation mark-error"></i>');    
                } else {
                    errorTab.html('');
                }

            };
        });

        if (errorNb > 0) {
            bootbox.alert('An error occurred, please check all fields marked by a <span class="placement-error"><i class="fa fa-exclamation mark-error"></i></span>'); 
        } else {
            console.log('good', mapping);
        }

    }

    this.refresh = function() {

    }

};

