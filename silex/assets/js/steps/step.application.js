var Steps = window.Steps || {};

Steps.Application = function(_ui) {

    $.extend(this, new Steps.Base());

    /* Load templates */
    this.requiredTemplates = ['steps.applications'];

    /* Basic stuff */
    this.ui = _ui,
    this.app = this.ui.getApp(),
    this.id = 'application';
    
    /* Elments used */
    this.elements = {};
    this.elements.content = this.ui.getStep(this.id).element;

    /* If not exists */
    if (! this.elements.content) return false;

    this.build = function() {
        var rendered = Mustache.render(app.getTemplate('steps.applications'));
        this.elements.content.append(rendered);
    }

};

