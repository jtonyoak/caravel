var Steps = window.Steps || {};

Steps.Imports = function(_ui) {
    var that = this;

    $.extend(this, new Steps.Base());

    /* Basic stuff */
    this.ui = _ui;
    this.app = this.ui.getApp();
    this.id = 'imports';
    
    /* Elements used */
    this.elements = {};
    this.elements.content = this.ui.getStep(this.id).element;

    /* Load templates */
    this.requiredTemplates = ['steps.imports.container', 'steps.imports.forms', 'steps.imports.lists', 'steps.imports.preview.modal'];

    /* If not exists */
    if (! this.elements.content) return false;

    this.init = function() {

    };

    this.build = function() {

        /* Reset html */

        this.elements.content.html('');

        /* Insert main template */

        var rendered = Mustache.render(that.app.getTemplate('steps.imports.container'));
        this.elements.content.append(rendered);

        /* Retreive all elements */
        this.elements.container   = $('.container', this.elements.content);
        this.elements.panel       = $(' > .row > .panel', this.elements.container);
        this.elements.panelBody   = $('.panel-body', this.elements.panel);
        this.elements.panelFooter = $('.panel-footer', this.elements.panel);
        this.elements.btnPrevStep = $('[data-toggle=step-imports-btn-prev-step]', this.elements.panelFooter);
        this.elements.btnNextStep = $('[data-toggle=step-imports-btn-next-step]', this.elements.panelFooter);

        /* */

        this.elements.btnPrevStep.click(function() {
            that.goToPrevStep();
        });

        this.elements.btnNextStep.click(function() {
            that.validate();
        });

        /* */
        
        var fieldset = $('<fieldset class="imports-forms col-md-3" style="margin-bottom: 40px">');
        var legend = $('<legend>Forms <i class="fa fa-question-circle" data-toggle=""></i>');

        fieldset.append(legend)
        this.elements.panelBody.append(fieldset);
        
        $.each(this.app.getForms(), function(i, j){
            if(j.selected) {
                var rendered = Mustache.render(that.app.getTemplate('steps.imports.forms'), {form: j});
                fieldset.append(rendered);

                var group = $('.imports-form[data-id=' + j.id + ']', fieldset);

                var uploadIcon = $('[data-toggle=imports-form-action-upload-icon]', group);
                var upload     = $('[data-toggle=imports-form-action-upload] input', group);
                var preview    = $('[data-toggle=imports-form-action-preview]', group);


                uploadIcon.click(function() {
                    upload.trigger('click');
                });

                upload.change(function() {

                    /* Prepare upload */
                    var data = new FormData();
                    $.each($(this)[0].files, function(i, file) {
                        data.append('file-'+i, file);
                    });

                    $.ajax({
                        url: '/api/file/' + that.app.getID() + '/' + j.id + '/',
                        data: data,
                        cache: false,
                        contentType: false,
                        processData: false,
                        async: false,
                        type: 'POST',

                        success: function(_data){
                            
                            if (_data.code && _data.code == 100) {

                                var info = $.ajax({
                                    url: '/api/file/' + that.app.getID() + '/' + j.id + '/',
                                    cache: false,
                                    async: false,
                                    type: 'GET'}).responseText;

                                info = $.parseJSON(info);

                                that.app.getForm(j.id).status = _data.code;
                                that.app.getForm(j.id).resource.columns = info.columns;
                                that.app.getForm(j.id).resource.sample = info.sample;

                                /* Generate unique ID */
                                var id = Math.floor((1 + Math.random()) * 0x10000000000).toString(16);

                                /* Generate table */
                                var table = new Utils.Table();
                                table.setCols(that.app.getForm(j.id).resource.sample.columns);
                                table.setRows(that.app.getForm(j.id).resource.sample.rows);
                                
                                /* Generate modal */
                                var modal = Mustache.render(that.app.getTemplate('steps.imports.preview.modal'), {
                                    id: id,
                                    title: 'Preview of ' + j.label,
                                    content: table.render(),
                                });

                                that.check();

                                $('body').append(modal);

                                preview.click(function() {
                                    $('#' + id).modal('show');
                                })
                            }
                        }
                    });
                });
            }
        });


        /* Lists */

        var fieldset = $('<fieldset class="step-imports-lists col-md-3">');
        var legend = $('<legend>Lists</legend>');

        fieldset.append(legend)
        this.elements.panelBody.append(fieldset);

        $.each(this.app.getConfig().lists, function(i, j){
            if(j.selected) {

                var rendered = Mustache.render(that.app.getTemplate('steps.imports.lists'), {
                    list: j,
                    forms: that.app.getForms()
                });

                fieldset.append(rendered);
                
                $('select', fieldset).change(function() {
                });
            }
        });

        /* History */

        var fieldset = $('<fieldset class="step-imports-lists col-md-3">');
        var legend = $('<legend>History</legend>');

        fieldset.append(legend)
        this.elements.panelBody.append(fieldset);

        /* History */

        var fieldset = $('<fieldset class="step-imports-lists col-md-3">');
        var legend = $('<legend>Referentiels</legend>');

        fieldset.append(legend)
        this.elements.panelBody.append(fieldset);

    }

    /* Kludge: Not the best way to achieve that but yeah, hackathon pressure ! */

    this.check = function() {
        var error = 0;

        $.each(this.app.getForms(), function(i, j){
            if(j.selected) {

                /* Check form-option-state [exclamation|check] */
                var group = $('.imports-form[data-id=' + j.id + ']');
                var icon = $('.form-option-state i', group);

                if(j.status) {
                    icon
                        .removeClass('fa-exclamation')
                        .addClass('fa-check');
                } else {
                    icon
                        .removeClass('fa-check')
                        .addClass('fa-exclamation');

                    error++;
                }

            }
        });

        return (error == 0) ? true : false;
    }

    this.refresh = function() {}

    this.validate = function() {
        if (this.check()) {
            this.goToNextStep();    
        } else {
            bootbox.alert('The form is not complete, please check for <i class="fa fa-exclamation"></i>'); 
        }
        
    }

}

