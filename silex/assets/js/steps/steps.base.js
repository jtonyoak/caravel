var Steps = window.Steps || {};

Steps.Base = function() {
    
    this.getRequiredTemplates = function() {
        return this.requiredTemplates;
    }

    this.show = function() {
        var that = this;
        this.build();

        this.elements.content.fadeIn(function() {
            that.refresh();
        });
    }

    this.hide = function(_callback) {
        this.elements.content.fadeOut(function() {
            if(_callback) _callback();
        });
    }

    /* Step Navigation */

    this.getPrevStep = function (){
        var that = this;
        var loop = true;
        var prevStepId = false;

        $.each(this.ui.getSteps(), function(i, j) {
            console.log('--', i);

            if(i == that.id) {
                return false;
            }

            prevStepId = i;

        })

        return prevStepId;
    }

    this.goToPrevStep = function() {

        console.log('this.goToPrevStep()');

        var that = this;

        this.hide(function(){
            var prevStep = that.getPrevStep();
            if (prevStep) {
                that.ui.getStep(prevStep).object.show();
            }
        })
    }

    this.getNextStep = function (){
        var that = this;
        var loop = true;
        var nextStepId = false;

        $.each(this.ui.getSteps(), function(i, j) {
            if(! loop) {
                nextStepId = i;
                return false;
            }
            if(i == that.id) {
                loop = false;
            }
        })

        return nextStepId;
    }

    this.goToNextStep = function() {
        var that = this;

        this.hide(function(){
            var nextStep = that.getNextStep();
            if (nextStep) {
                that.ui.getStep(nextStep).object.show();
            }
        })
    }
}
