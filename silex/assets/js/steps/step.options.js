var Steps = window.Steps || {};

Steps.Options = function(_ui) {

    $.extend(this, new Steps.Base());

    this.ui = _ui;
    this.app = this.ui.getApp();

    this.id = 'options',
    this.built = false;
    
    this.elements = {};
    this.elements.content = this.ui.getStep(this.id).element;

    this.requiredTemplates = ['steps.options.panel'];

    var that = this;

    if (! this.elements.content) return false;

    this.build = function() {
        
        /* Reset html */

        this.elements.content.html('');

        /* Insert main template */

        var rendered = Mustache.render(this.app.getTemplate('steps.options.panel'), {
            forms: this.app.getForms(),
            lists: this.app.getLists()
        });

        this.elements.content.append(rendered);

        /* Retreive used elements */

        this.elements.container   = $('.container', this.elements.content);
        this.elements.panel       = $(' > .row > .panel', this.elements.container);
        this.elements.panelBody   = $('.panel-body', this.elements.panel);
        this.elements.panelFooter = $('.panel-footer', this.elements.panel);
        this.elements.btnPrevStep = $('[data-toggle=step-options-btn-prev-step]', this.elements.panelFooter);
        this.elements.btnNextStep = $('[data-toggle=step-options-btn-next-step]', this.elements.panelFooter);

        this.elements.optionsForms = $('.options-forms .form-option', this.elements.panelBody);
        this.elements.optionsLists = $('.options-lists .list-option', this.elements.panelBody);

        /* */
           
        this.elements.btnNextStep.click(function() {
            that.validate($(this));
        });

        /* Forms */

        $.each(this.elements.optionsForms, function(i, j) {
            var option = $(j),
                id = option.data('id'),
                form = that.app.getForm(id),
                elements = {
                    selected: $('.form-option-selected', option)
                }        

            if(that.app.getForm(id).supported) {

                option.addClass('supported')

                /* */
                if(that.app.getForm(id).selected) {
                    elements.selected.removeClass('fa-circle-o').addClass('fa-circle');
                } else {
                    elements.selected.removeClass('fa-circle').addClass('fa-circle-o');
                }

                /* Click on formbox */
                option.click(function() {
                    that.app.getForm(id).selected = ! that.app.getForm(id).selected;

                    if(that.app.getForm(id).selected) {
                        elements.selected.removeClass('fa-circle-o').addClass('fa-circle');
                    } else {
                        elements.selected.removeClass('fa-circle').addClass('fa-circle-o');
                    }
                });

            } else {
                option
                    .addClass('unsupported')
                    .tooltip({
                        title: 'Not supported for this presentation',
                        placement: 'bottom auto'
                    });
            }

        });

        /* Lists */
        
        $.each(this.elements.optionsLists, function(i, j) {
            var option = $(j),
                id = option.data('id'),
                list = that.app.getList(id),
                elements = {
                    selected: $('.list-option-selected', option)
                }        

            if(that.app.getList(id).supported) {

                option.addClass('supported')

                /* */
                if(that.app.getList(id).selected) {
                    elements.selected.removeClass('fa-circle-o').addClass('fa-circle');
                } else {
                    elements.selected.removeClass('fa-circle').addClass('fa-circle-o');
                }

                /* Click on formbox */
                option.click(function() {
                    that.app.getList(id).selected = ! that.app.getList(id).selected;

                    if(that.app.getList(id).selected) {
                        elements.selected.removeClass('fa-circle-o').addClass('fa-circle');
                    } else {
                        elements.selected.removeClass('fa-circle').addClass('fa-circle-o');
                    }
                });

            } else {
                option
                    .addClass('unsupported')
                    .tooltip({
                        title: 'Not supported for this presentation',
                        placement: 'bottom auto'
                    });
            }

        });

        this.built = true;

    }

    /* Refresh in case of data changes */

    this.refresh = function() {
        console.log('refreshed!')
    }

    /* Check the form */

    this.validate = function(btn) {
        var countForm = 0;

        $.each(this.app.getForms(), function(i, j){
            if(j.selected && j.supported) countForm++
        })

        this.goToNextStep();

    }

}

